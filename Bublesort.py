__author__ = 'Iliya'

import cProfile

inputmassiv  = [23,543,6,1,2,44,53,68,90,53,67]
inputmassiv_again = inputmassiv.copy()
input_massiv_1=[1,2,3]
input_massiv_2=[1,2,3]

def profile(function) :

    def wrapper( *args, **kwargs):
        profile_filename = function.__name__ + '.prof'
        profiler = cProfile.Profile()
        result = profiler.runcall(function, *args, **kwargs)
        profiler.dump_stats(profile_filename)
        return result
    return wrapper

@profile
def bubleflag(massiv) :
    capacity = len(massiv)
    while capacity > 1 :
        k = 0
        for j in range(capacity-1) :
             if massiv[j] > massiv[j+1] :
                 massiv[j],massiv[j+1] = massiv[j+1],massiv[j]
                 k+=1
        if k == 0 : break
        capacity -=1


@profile
def buble(massiv) :
    capacity = len(massiv)
    while capacity > 1 :
        for j in range(capacity-1) :
             if massiv[j] > massiv[j+1] :
                 massiv[j],massiv[j+1] = massiv[j+1],massiv[j]
        capacity -=1


def printarr(massiv):
    elements = len(massiv)
    for i in range (elements):
        print(massiv[i])

bubleflag(inputmassiv)
bubleflag(input_massiv_1)

buble(inputmassiv_again)
buble(input_massiv_2)


printarr(inputmassiv)
printarr(input_massiv_1)
